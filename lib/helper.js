const fs = require('fs');
// const moment = require('moment-timezone');
const moment = require('moment');
const randomstring = require('randomstring');
const path = require('path');
const multer = require('multer');
const async = require('async');
const thumb = require('node-thumbnail').thumb;
const team = require('../models/team');
const poolPlayerGame = require('../models/poolPlayerGame');
const standing = require("../models/standing");
const pools = require("../models/pool");
const user = require('../models/user');
const availableTeams = require('../models/availableTeams');
var mongoose = require('mongoose');
const timezone = 'America/New_York';

const fileUpload = function (req, res, path, type, cb) {
  console.log("isdfi ");

  let storage = multer.diskStorage({
    destination: function (req, file, callback) {
      if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mov') {
        createDir(`./uploads/${path}`);
        callback(null, `./uploads/${path}`);
      } else if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        createDir(`./uploads/${path}`);
        callback(null, `./uploads/${path}`);
      } else {
        callback('Mime type not supported');
      }
    },
    filename: function (req, file, callback) {
      if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mov') {
        callback(null, randomstring.generate(8) + '.mp4');
      } else if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        callback(null, randomstring.generate(8) + '.jpg');
      } else {
        callback('Mime type not supported filename')
      }
    }
  });
  let upload;
  if (type == 'single') {
    upload = multer({
      storage: storage
    }).single('files')
    upload(req, res, function (err) {
      createThumbnail(req.file, path, (fName) => {
        var ext = fName.split('.');
        cb(`/${path}/${ext[0]}`)
      })
    })

  } else if (type == 'any') {
    upload = multer({
      storage: storage
    }).any()
    upload(req, res, function (err) {
      let imgArr = [];
      async.eachOfSeries(req.files, function (t, i, callback) {
        createThumbnail(t, path, (fName) => {
          var ext = fName.split('.');
          imgArr.push(`/${path}/${ext[0]}`)
          callback()
        })
      }, () => {
        console.log('done!');
        cb(imgArr)
      });
    })
  }
}

const createThumbnail = (t, path, cb) => {

  console.log("t", t);
  var ext = t.filename.split('.');

  thumb({
    prefix: '',
    suffix: '_small',
    source: `./uploads/${path}` + '/' + t.filename,
    destination: `./uploads/${path}`,
    width: 100,
    overwrite: true,
    concurrency: 4,
    basename: ext[0]
  }).then(function () {
    console.log('Success  width: 100');
  }).catch(function (e) {
    console.log('Error  width: 100', e.toString());
  });

  thumb({
    prefix: '',
    suffix: '_medium',
    source: `./uploads/${path}` + '/' + t.filename,
    destination: `./uploads/${path}`,
    width: 600,
    overwrite: true,
    concurrency: 4,
    basename: ext[0]
  }).then(function () {
    console.log('Success  width: 600');
  }).catch(function (e) {
    console.log('Error  width: 600', e.toString());
  });

  cb(t.filename);
}

const createDir = (targetDir) => {
  const path = require('path');
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : '';
  targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(parentDir, childDir);
    if (!fs.existsSync(curDir)) {
      fs.mkdirSync(curDir);
    }
    return curDir;
  }, initDir);
}

const writeFile = (errLog, folder) => {
  var updatedAt = new Date();
  var date = new Date(updatedAt.getTime() + moment.tz('Asia/Kolkata').utcOffset() * 60000);
  errLog += "########## \r\n";
  createDir(`./${folder}`);
  fs.appendFileSync(`./${folder}/${date.toISOString().slice(0, 10)}.txt`, errLog + "\r\n", function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
}

/* const convertToServerTimeZone = (cb) => {
  //EST
  offset = -5.0;
  clientDate = new Date();
  utc = clientDate.getTime() + (clientDate.getTimezoneOffset() * 60000);
  serverDate = new Date(utc + (3600000*offset));
  return cb(null, new Date(serverDate))
} */

function getMonthDateRange(year, month, cb) {
  console.log('inside function', year, month);

  var moment = require('moment');

  // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
  // array is 'year', 'month', 'day', etc
  var startDate = moment([year, month]);

  // Clone the value before .endOf()
  var endDate = moment(startDate).endOf('month');

  // just for demonstration:
  console.log(startDate);
  console.log(endDate);

  // make sure to call toDate() for plain JavaScript date type
  return cb(null, { endDate: moment(endDate).format(), startDate: moment(startDate).format() })
  // return { start: startDate, end: endDate };
}

const getTeam = (awayTeam, homeTeam, cb) => {
  console.log('awayTeam', awayTeam);
  console.log('homeTeam', homeTeam);
  team.find({ $or: [{ teamId: awayTeam }, { teamId: homeTeam }] })
    .exec((error, Response) => {
      // console.log('error',error);
      // console.log('Response',Response);
      // process.exit();
      if (error) {
        cb(error)
      } else {
        cb(null, Response)
      }
    })
}

const getPoolPlayerGame = (poolId, userId, matchData, cb) => {
  poolPlayerGame.find({
    $and: [
      { playerId: userId },
      { poolId: poolId },
    ]
  })
    .exec((err, res) => {
      console.log("err", err)
      if (err) {
        cb(err)
      } else {
        let teamSelected = Array(matchData.length).fill("");
        let valueSelected = Array(matchData.length).fill("");
        async.eachOfSeries((matchData), (eachData, key1, callback) => {
          let temp = eachData.matches.map((val, key) => {
            let index = res.findIndex((e) => e.matchId == val.gameId);
            if (index > -1) {
              teamSelected[key1] = val;
              valueSelected[key1] = res[index].teamSelected
              return res[index].valueSelected;
            } else {
              return "";
            }
          });
          callback()
        }, (error) => {
          if (error) {
            cb(null, valueSelected, teamSelected)
          }
          cb(null, valueSelected, teamSelected)
        })
      }
    })
}

const calculateStandings = () => {
  pools.find()
    .exec((err, poolData) => {
      if (err) {
        console.log('error while fetching pools standing')
        return;
      }
      if (!poolData) {
        console.log('poolData fetching standing', poolData)
        return;
      }
      else {
        async.eachOfSeries(poolData, (pool, key, callback) => {
          console.log("pool.....", pool)
          poolPlayerGame.aggregate([
            {
              $match: {
                poolId: pool._id
              }
            },
            {
              $project: {
                _id: 1, poolId: 1, playerId: 1, score: 1

              }
            },
            { $unwind: '$poolId' },
            {
              "$group": {
                _id: '$playerId',
                pool: {
                  $push: '$poolId'
                },
                win: {
                  $push: {
                    $eq: ["$score", 1]
                  }
                },
                loss: {
                  $push: {
                    $eq: ["$score", 0]
                  }
                },
              }
            },
            {
              $project: {
                _id: '$_id',
                pools: '$pool',
                wins: {
                  $filter: {
                    input: '$win',
                    as: 'each',
                    cond: { $eq: ['$$each', true] }
                  }
                },
                losses: {
                  $filter: {
                    input: '$loss',
                    as: 'each',
                    cond: { $eq: ['$$each', true] }
                  }
                }
              }
            },
            {
              $project: {
                poolId: { $arrayElemAt: ["$pools", 0] },
                wins: { $size: "$wins" },
                losses: { $size: "$losses" }
              }
            }, { $lookup: { from: 'users', localField: '_id', foreignField: '_id', as: 'player' } },
            { $sort: { 'wins': -1 } }
          ]).exec((error, user) => {
            console.log('matchDetail user', user)
            if (error) {
              console.log('error', error);
              callback();
            }
            if (!user) {
              console.log('user', user)
              callback();
            }
            if (user.length == 0) {
              console.log('user', user)
              callback();
            }
            else {
              async.eachOfSeries(user, (eachUser, i, cb) => {
                console.log("eachUser", eachUser, mongoose.Types.ObjectId(eachUser._id), mongoose.Types.ObjectId(pool._id));

                standing.update({ playerId: mongoose.Types.ObjectId(eachUser._id), poolId: mongoose.Types.ObjectId(pool._id), status: true }, { wins: eachUser.wins, loss: eachUser.losses }, { upsert: true })
                  .exec((error1, standing) => {
                    console.log("error1,standing", error1, standing);
                    cb();
                  })
              }, (err) => {
                console.log('donq with calculation');
                callback();
              })
            }
          })
        }, (err) => {
          if (err) {
            return;
          } else {
            return;
          }
        })
      }
    })

}

const getAvailableTeams = (poolId, userId, cb) => {
  availableTeams.find({ poolId: poolId, userId: userId }, (err, data) => {
    console.log('getAvailableTeams...', data)
    if (err) {
      cb(null, [], [])
    } else {
      if (data.length > 0 && data[0].selected.length == 30 && moment().format('D') == 31) {
        console.log('inside length =30');
        availableTeams.findOneAndUpdate({ poolId: poolId, userId: userId }, { $set: { selected: [] } }, (err, updatedata) => {
          console.log('err', err);
          console.log('updatedata', updatedata);
        })
      }
      cb(null, [], data)
    }
  })

}
module.exports = {
  createDir,
  writeFile,
  fileUpload,
  createThumbnail,
  getMonthDateRange,
  getTeam,
  getPoolPlayerGame,
  calculateStandings,
  getAvailableTeams,
  timezone
}