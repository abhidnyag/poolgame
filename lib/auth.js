var config = require("./config");
var moment = require('moment-timezone');
var jwt = require('jsonwebtoken');
var helper = require("../lib/helper");
let user = require('../models/user');

const isAuthenticated = function (data) {
  return (req, res, next) => {

    let decoded = jwt.decode(req.headers.token, 'poolGame_19_04_2019_');

    var updatedAt = new Date();
    let errLog = '\r\n########## \r\n' + new Date(updatedAt.getTime() + moment.tz('Asia/Kolkata').utcOffset() * 60000) + "\r\n";

    errLog += `decoded ---- ${JSON.stringify(decoded)} \r\n`;
    errLog += `data inside isAuthenticated---- ${JSON.stringify(data)} \r\n`;
    //check whether the path requested by the user is present in config.js file if not send error.
    let index = config.acl.findIndex((e) => e.property == data);
    if (index < 0) {
      helper.writeFile(errLog, 'accessLog');
      return res.status(200).json({
        error: true,
        title: 'Authorization Required'
      });
    }

    // If role is admin allow access.
    if (decoded.role == "admin") {
      return next()
    }

    if (config.acl[index].permission.indexOf('*') > -1) {
      // all user can access the route
      return next()
    }
    else {
      if (!decoded || decoded == undefined) {
        helper.writeFile(errLog, 'accessLog');
        return res.status(200).json({
          error: true,
          title: 'Invalid access token.'
        });
      }

      if (config.acl[index].permission.indexOf(decoded.role) > -1) {
        // Only authenticated user having specific permission can access the route.
        return next()
      } else {
        helper.writeFile(errLog, 'accessLog');
        return res.status(200).json({
          error: true,
          title: 'Authorization Required'
        });
      }
    }
  }
}

const authenticateUser = async (req, res, next) => {
  const token = req.headers.token;
  const decoded = jwt.decode(token, "poolGame_19_04_2019_");

  try {
    const userData = await user.findById(decoded.userId).exec();

    if (!userData || userData == undefined) {
      return res.status(200).json({
        title: 'user not found',
        error: true,
      });
    }
    if (parseInt(userData.isBlocked) == 1) {
      return res.status(200).json({
        title: 'You are blocked.Please contact admin',
        error: true,
        detail: "invalid Login"
      });
    }
    if (userData.isLoggedIn == false) {
      return res.status(200).json({
        title: 'Authorization required',
        error: true,
        detail: "invalid Login"
      });
    }
    req.body.userData = userData;
    return next(null, userData);
  }
  catch (error) {
    return res.status(200).json({
      title: 'Authorization required.',
      error: true,
      detail: error
    });
  }

}



module.exports = {
  isAuthenticated,
  authenticateUser
}