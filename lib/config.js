/* * - represent access for all type of user
[] - represent access for admin only.
['admin', 'user', ''] - n type of user can be added so that they can access the route */

let acl = [
  {
    permission: ["admin", "user"],// type of users
    property: "users" // name of the api passed in middleware.
  },
  {
    permission: ["admin"],
    property: "addUser"
  },
  {
    permission: ['*'],
    property: "login"
  },
  {
    permission: ['*'],
    property: "editUser"
  },
  {
    permission: ['*'],
    property: "changePassword"
  },
  {
    permission: [],
    property: "getAllUsers"
  },
  {
    permission: ['*'],
    property: "createPool"
  },
  {
    permission: ['*'],
    property: "getAllPools"
  },
  {
    permission: [],
    property: "getMlbTeams"
  },
  {
    permission: [],
    property: "getTeams"
  },
  {
    permission: ['*'],
    property: "profile"
  },
  {
    permission: ['*'],
    property: "getMonthlyMatches"
  },
  {
    permission: ['*'],
    property: "getGames"
  },
  {
    permission: [],
    property: "updateGameDetail"
  },
  {
    permission: ['*'],
    property: "uploadProfile"
  },
  {
    permission: ['*'],
    property: "poolDetail"
  },
  {
    permission: [],
    property: "addgameFormat"
  },
  {
    permission: ['*'],
    property: "getgameFormat"
  },
  {
    permission: ['*'],
    property: "joinPool"
  },
  {
    permission: ['*'],
    property: "addPicks"
  },
  {
    permission: ['*'],
    property: "getMonthlyMatches"
  },
  {
    permission: [],
    property: "changeUserStatus"
  },
  {
    permission: [],
    property: "updatePoolDetail"
  },
  {
    permission: ['*'],
    property: "addInterest"
  },
  {
    permission: ['*'],
    property: "fetchLeaderboardData"
  },
  {
    permission: ['*'],
    property: "matchDetail"
  },
  {
    permission: ['*'],
    property: "removeUser"
  }
]

module.exports = {
  acl
}