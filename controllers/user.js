const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const async = require('async');
const nodemailer = require('nodemailer');
const randomstring = require('randomstring');
const request = require("request");
const moment = require('moment');
var CronJob = require('cron').CronJob;
const momentTimezone = require('moment-timezone');
var mongoose = require('mongoose');

const chat = require('../models/chat');
const pool = require('../models/pool');
const team = require('../models/team');
const match = require('../models/match');
const user = require('../models/user');
const gameFormat = require("../models/gameFormat");
const poolPlayerGame = require("../models/poolPlayerGame");
const standing = require("../models/standing");
const helper = require('../lib/helper');
const availableTeams = require('../models/availableTeams');

// America/New_York
var smtpTransport = nodemailer.createTransport({
    tls: { rejectUnauthorized: false },
    secureConnection: false,
    service: "gmail",
    host: "smtp.gmail.com",
    port: 587,
    requiresAuth: true,
    auth: {
        user: process.env.mail_username,
        pass: process.env.mail_password
    }
});


const signup = function (req, res, imageArr) {
    console.log("req....", req.body)
    let errors = req.validationErrors();
    console.log('errors', errors);

    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        user.find({ email: req.body.email }).exec((error, userData) => {
            if (userData.length > 0) {
                return res.status(200).json({
                    title: "User email ID already exists.",
                    details: error,
                    error: true,
                });
            } else {
                req.body.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
                let newUser = new user(req.body);
                newUser.save((err, userdata) => {
                    if (err) {
                        return res.status(200).json({
                            title: "Something went wrong please try again.",
                            details: err,
                            error: true,
                        });
                    } else {
                        var token = jwt.sign({
                            email: userdata.email,
                            userId: userdata.id,
                            role: userdata.role,
                            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60 * 1000),
                        }, "poolGame_19_04_2019_");

                        return res.status(200).json({
                            title: "Signup successful.",
                            token: token,
                            userdata: userdata,
                            error: false,
                        });
                    }
                });
            }

        })

    }
}


const editProfile = (req, res) => {
    let user = req.body.userData;
    user.name = req.body.name;
    user.mobile = req.body.mobile;
    user.dob = req.body.dob;
    user.gender = req.body.gender;
    user.address = req.body.address;
    user.age = req.body.age;
    user.save((err, user) => {
        if (err) {
            return res.status(200).json({
                title: 'An error occurred',
                error: true,
                detail: err
            });
        }
        res.status(200).json({
            title: 'User updated succesfully.',
            error: false,
            user: user
        });
    });
}

const forgotPassword = (req, res) => {
    console.log("req.body", req.body);

    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });
                done(err, token);
            });
        },
        function (token, done) {
            user.findOne({ email: req.body.email }, function (err, user) {
                if (!user) {
                    // req.flash('error_msg', 'No account with that email address exists.');
                    return res.status(200).json({
                        title: 'No account with that email address exists.',
                        error: true,
                        detail: err
                    });
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
                user.save(function (errr) {
                    console.log('user token save')
                    done(errr, token, user);
                });
            });
        },
        function (token, user, done) {
            var mailOptions = {
                to: user.email,
                from: 'sachin.yadav@infiny.in',
                subject: 'PoolGame Password Reset',
                html:
                    '<p>' +
                    'You are receiving this because you (or someone else) have requested the reset of the password for your account. <br>' +
                    'Please click on the following link, or paste this into your browser to complete the process:<br>' +
                    /* '<a href= "http://' + req.headers.host.split(':')[0] + '/poolgameweb/#/reset/' + token + '">Click here' + '</a>' + */
                    '<a href= "http://poolsidesports.com/#/reset/' + token + '">Click here' + '</a>' +
                    /*  '<a href= "http://localhost:3001/#/reset/' + token + '">Click here' + '</a>' + */
                    '</p>'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                //   req.flash('success_msg', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                done(err, 'done');
            });
        }
    ], function (err) {
        if (err) return next(err);
        return res.status(200).json({
            title: 'Forgot password link sent successfully.',
            error: false,
            detail: err
        });
    });
}

const resetToken = (req, res) => {
    async.waterfall([
        function (done) {
            user.findOne({ resetPasswordToken: req.body.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
                console.log("user", user, 'err', err);
                if (!user) {
                    return res.status(200).json({
                        title: 'Password reset token is invalid or has expired.',
                        error: true,
                        detail: err
                    });
                }
                console.log("req.body.password", req.body.password);

                user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                user.save(function (err, user) {
                    done(err, user);
                });
            });
        },
        function (user, done) {
            var mailOptions = {
                to: user.email,
                from: 'sachin.yadav@infiny.in',
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                    'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                done(err);
            });
        }
    ], function (err) {
        return res.status(200).json({
            title: 'Success! Your password has been changed.',
            error: false,
            detail: err
        });
    });
}

const changePassword = (req, res) => {
    let user = req.body.userData;

    bcrypt.compare(req.body.oldPassword, user.password, function (err, result) {
        // result == true
        if (result) {
            user.password = bcrypt.hashSync(req.body.newPassword, bcrypt.genSaltSync(10));
            user.save((error, savedUser) => {
                if (error) {
                    return res.status(200).json({
                        title: 'Something went wrong in change password.',
                        error: true,
                        detail: error
                    });
                } else {
                    return res.status(200).json({
                        title: 'Password changed successfully.',
                        error: false,
                        detail: savedUser
                    });
                }
            });
        } else {
            return res.status(200).json({
                title: 'Password you entered is incorrect.',
                error: true,
                detail: err
            });
        }
    });
}

const createPool = (req, res) => {
    console.log("req.body", req.body);
    let code = randomstring.generate({
        length: 6,
        charset: 'numeric'
    });
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: errors[0].msg,
            errors: errors
        });
    } else {
        let year = new Date().getFullYear();
        helper.getMonthDateRange(year, req.body.month, (err, date) => {
            console.log('date', date);
            let newDate = date.endDate;

            if (newDate == 'Invalid date') {
                newDate = momentTimezone.tz(new Date(), helper.timezone).format("YYYY-MM-DD")
            }

            const map = ((req.body.noPickDays.findIndex(e => e == '') < 0) && req.body.noPickDays.length) > 0 ? req.body.noPickDays.map(x => {
                if (momentTimezone.tz(new Date(x.date), helper.timezone).format("YYYY-MM-DD") == 'Invalid date') {
                    return null
                } else {
                    return momentTimezone.tz(new Date(x.date), helper.timezone).format("YYYY-MM-DD");
                }
            }).filter(e => {
                return e != null;
            }) : [];

            console.log('mape,', map);
            var poolData = {
                poolName: req.body.poolName,
                maxPlayers: req.body.maxPlayers,
                owner: req.body.userData._id,
                month: newDate,
                inviteCode: code,
                noPickDays: map,
                gameType: req.body.gameType
            }
            console.log('poolData', poolData)
            const poolCreated = new pool(poolData);
            poolCreated.save((error, response) => {
                console.log("error", error, response)
                if (response) {
                    let newStanding = new standing({
                        playerId: req.body.userData._id,
                        poolId: response._id
                    })
                    newStanding.save((err, data) => {

                        const TempAvailableTeams = new availableTeams({
                            userId: req.body.userData._id,
                            poolId: response._id,
                            selected: [],
                            selectionForMonth: momentTimezone.tz(new Date(), helper.timezone).format('DD/MM/YYYY')
                        })
                        TempAvailableTeams.save((err, data) => {
                            console.log("error", err);
                            console.log("data", data);
                        })

                        return res.status(200).json({
                            title: 'Pool created successfully.',
                            error: false,
                            detail: poolCreated
                        });
                    })
                } else {
                    return res.status(200).json({
                        title: 'Error while creating Pool.',
                        error: true,
                        detail: error
                    });
                }
            });
        });
    }
}

const getPools = (req, res) => {
    if (req.body.userData.role === 'user') {
        standing.aggregate([
            { $match: { playerId: req.body.userData._id, status: true } },
            {
                $group: {
                    _id: "$playerId",
                    pools: { $push: { poolId: '$poolId' } },
                }
            },
            { $lookup: { from: 'pools', localField: 'pools.poolId', foreignField: '_id', as: 'pools' } },
            { $lookup: { from: 'users', localField: '_id', foreignField: '_id', as: 'player' } },
            { $unwind: "$player" },
            { $unwind: "$pools" },
            { $lookup: { from: 'users', localField: 'pools.winner', foreignField: '_id', as: 'winner' } },
            {
                $group: {
                    _id: "$_id",
                    pools: { $push: "$pools" },
                    winner: { $push: "$winner" }
                }
            },
        ])
            .exec((err, pooldata) => {
                console.log("here in findall", pooldata)
                if (err) {
                    return res.status(200).json({
                        title: 'Something went wrong',
                        error: true,
                        detail: err
                    });
                }
                if (!pooldata) {
                    return res.status(200).json({
                        title: 'No pooldata found',
                        error: true,
                        detail: pooldata
                    });
                } else {
                    return res.status(200).json({
                        title: 'pooldata data.',
                        error: false,
                        detail: pooldata
                    });
                }
            })
    } else {
        pool.find()
            .populate('owner', 'name email username')
            .populate('gameType')
            .exec((err, pooldata) => {
                console.log("here in findall", pooldata)
                if (err) {
                    return res.status(200).json({
                        title: 'Something went wrong',
                        error: true,
                        detail: err
                    });
                }
                if (!pooldata) {
                    return res.status(200).json({
                        title: 'No pooldata found',
                        error: true,
                        detail: pooldata
                    });
                } else {
                    return res.status(200).json({
                        title: 'pooldata data.',
                        error: false,
                        detail: pooldata
                    });
                }
            })
    }

}

const getAllUser = (req, res) => {
    user.find({ role: "user" })
        .exec((err, users) => {
            if (err) {
                return res.status(200).json({
                    title: 'Something went wrong',
                    error: true,
                    detail: err
                });
            }
            if (!users) {
                return res.status(200).json({
                    title: 'No user found',
                    error: true,
                    detail: users
                });
            } else {
                return res.status(200).json({
                    title: 'Users data.',
                    error: false,
                    detail: users
                });
            }
        })
}

const getMlbTeams = (req, res) => {
    request({
        url: `https://api.sportsdata.io/v3/mlb/scores/json/teams?Key=${process.env.apiKey}`,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            async.eachOfSeries(body, (eachData, key, cb) => {
                team.findOrCreate({ teamId: eachData.TeamID, city: eachData.City, shortCode: eachData.Key, logoUrl: eachData.WikipediaLogoUrl, division: eachData.Division, teamName: eachData.Name }, function (err, click) {
                    cb();
                })
            }, (err) => {
                if (err) {
                    return res.status(200).json({
                        title: body.message,
                        error: true,
                        detail: err
                    });
                } else {
                    return res.status(200).json({
                        title: 'Teams fetched sucessfully',
                        error: false,
                        detail: body
                    });
                }
            })
        } else {
            return res.status(200).json({
                title: body.message,
                error: true,
                detail: error
            });
        }
    });
}

const getMlbGames = (req, res) => {
    request({
        url: `https://api.sportsdata.io/v3/mlb/scores/json/Games/2019?Key=${process.env.apiKey}`,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            async.eachOfSeries(body, (eachData, key, cb) => {
                helper.getTeam(eachData.AwayTeamID, eachData.HomeTeamID, (error, res) => {
                    let AwayTeamIndex = res.findIndex((e) => e.teamId == eachData.AwayTeamID);
                    let HomeTeamIndex = res.findIndex((e) => e.teamId == eachData.HomeTeamID);
                    if (AwayTeamIndex > -1 && HomeTeamIndex > -1) {
                        console.log('inside index', AwayTeamIndex, HomeTeamIndex);
                        let AwayTeamId = res[AwayTeamIndex]._id;
                        let HomeTeamId = res[HomeTeamIndex]._id;
                        console.log('inside index', AwayTeamId, HomeTeamId);
                        match.findOrCreate({ gameId: eachData.GameID, seasonType: eachData.SeasonType, season: eachData.Season, awayTeam: AwayTeamId, homeTeam: HomeTeamId, day: momentTimezone(new Date(eachData.Day)), dateTime: momentTimezone(new Date(eachData.DateTime)) }, function (err, matchData) {
                            console.log('matchData', matchData, err)
                            if (eachData.RescheduledGameID != matchData.reScheduledGameId) {
                                matchData.reScheduledGameId = eachData.RescheduledGameID,
                                    matchData.status = eachData.Status
                                matchData.save((err, newData) => {
                                    console.log('newData', newData);
                                });
                            }
                            cb();
                        });
                    } else {
                        console.log('else index', AwayTeamIndex, HomeTeamIndex);
                        cb();
                    }
                });
            }, (err) => {
                /* if(err) {
                    return res.status(200).json({
                        title: body.message,
                        error: true,
                        detail: err
                    });
                } else {
                    return res.status(200).json({
                        title: 'Games fetched sucessfully',
                        error: false,
                        detail: body
                    });
                } */
            })
        } else {
            /* return res.status(200).json({
                title: body.message,
                error: true,
                detail: error
            }); */
        }
    });
    return res.status(200).json({
        title: 'Fetching Game',
        error: false,
        detail: ''
    });
}

const getTeams = (req, res) => {
    team.find()
        .exec((error, teams) => {
            if (error) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error
                });
            } else {
                return res.status(200).json({
                    title: 'Teams fetched sucessfully',
                    error: false,
                    detail: teams
                });
            }
        })
}

const getProfile = (req, res) => {
    let img = null;
    if (req.body.userData && req.body.userData.profileImage != null) {
        img = 'http://poolsidesports.com/users/' + req.body.userData.profileImage + '.png';
    }

    user.findById(req.body.userData._id)
        .populate('interest', 'discription title code')
        .exec((error, response) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong please try again.',
                    error: true,
                    detail: error
                });
            }
            return res.status(200).json({
                title: 'User profile fetched sucessfully',
                error: false,
                detail: req.body.userData,
                profileImage: img
            });
        })
}

/* const getMonthlyMatches = (req, res) => {
    let year = new Date().getFullYear();
    let month = new Date().getMonth();
    helper.getMonthDateRange(year, month, (err, date) => {
        console.log("date", date)
        poolPlayerGame.aggregate([
            {
                $match: {
                    playerId: mongoose.Types.ObjectId(req.params.playerId),
                    poolId: mongoose.Types.ObjectId(req.params.poolId),
                    matchDate : momentTimezone.tz(new Date(), helper.timezone).utc().format('YYYY-MM-DD')
                }
            },
            { $lookup: { from: 'matches', localField: 'matchId', foreignField: 'gameId', as: 'match' } },
            { $lookup: { from: 'teams', localField: 'match.homeTeam', foreignField: '_id', as: 'homeTeam' } },
            { $lookup: { from: 'teams', localField: 'match.awayTeam', foreignField: '_id', as: 'awayTeam' } },
            { $sort: { matchDate: -1 } }
        ])
        .exec((error, matchData) => {
            console.log('matchData', matchData)
            console.log('error', error)
            process.exit();
            if (error) {
                return res.status(200).json({
                    title: 'Something Went wrong, please try again.',
                    error: true,
                    detail: error
                });
            }
            if (!matchData) {
                return res.status(200).json({
                    title: 'No match data found',
                    error: false,
                    detail: matchData
                });
            }
            else {
                helper.getAvailableTeams(req.params.poolId, req.params.playerId, (error, valueSelected, teamSelected) => {
                    console.log('valueSelected', valueSelected);
                    console.log('teamSelected', teamSelected);
                    return res.status(200).json({
                        title: 'Match data fetched sucessfully',
                        error: false,
                        detail: matchData,
                        valueSelected: valueSelected,
                        teamSelected: teamSelected
                    });
                });
            }
        })
    })
} */

const getMonthlyMatches = (req, res) => {
    let year = new Date().getFullYear();
    let month = new Date().getMonth();
    let poolMatchData = {}
    async.waterfall([
        function (callback) {
            poolPlayerGame.aggregate([
                {
                    $match: {
                        playerId: mongoose.Types.ObjectId(req.params.playerId),
                        poolId: mongoose.Types.ObjectId(req.params.poolId),
                        matchDate: momentTimezone.tz(new Date(), helper.timezone).utc().format('YYYY-MM-DD')
                    }
                },
                { $lookup: { from: 'matches', localField: 'matchId', foreignField: 'gameId', as: 'match' } },
                { $lookup: { from: 'teams', localField: 'match.homeTeam', foreignField: '_id', as: 'homeTeam' } },
                { $lookup: { from: 'teams', localField: 'match.awayTeam', foreignField: '_id', as: 'awayTeam' } },
                { $sort: { matchDate: -1 } }
            ])
                .exec((error, matchData) => {
                    console.log('matchData', matchData)
                    console.log('error', error)

                    if (error) {
                        return res.status(200).json({
                            title: 'Something Went wrong, please try again.',
                            error: true,
                            detail: error
                        });
                    }
                    if (!matchData) {
                        return res.status(200).json({
                            title: 'No match data found',
                            error: false,
                            detail: matchData
                        });
                    }
                    else {
                        poolMatchData.today = matchData;
                        callback(null);
                    }
                })
        },
        function (callback) {
            poolPlayerGame.aggregate([
                {
                    $match: {
                        playerId: mongoose.Types.ObjectId(req.params.playerId),
                        poolId: mongoose.Types.ObjectId(req.params.poolId),
                        matchDate: { $gt: momentTimezone.tz(new Date(), helper.timezone).utc().format('YYYY-MM-DD') }
                    }
                },
                { $lookup: { from: 'matches', localField: 'matchId', foreignField: 'gameId', as: 'match' } },
                { $lookup: { from: 'teams', localField: 'match.homeTeam', foreignField: '_id', as: 'homeTeam' } },
                { $lookup: { from: 'teams', localField: 'match.awayTeam', foreignField: '_id', as: 'awayTeam' } },
                { $sort: { matchDate: -1 } }
            ])
                .exec((error, matchData) => {
                    console.log('matchData', matchData)
                    console.log('error', error)

                    if (error) {
                        return res.status(200).json({
                            title: 'Something Went wrong, please try again.',
                            error: true,
                            detail: error
                        });
                    }
                    if (!matchData) {
                        return res.status(200).json({
                            title: 'No match data found',
                            error: false,
                            detail: matchData
                        });
                    }
                    else {
                        poolMatchData.upcoming = matchData;
                        callback(null);
                    }
                })
        },
        function (callback) {
            poolPlayerGame.aggregate([
                {
                    $match: {
                        playerId: mongoose.Types.ObjectId(req.params.playerId),
                        poolId: mongoose.Types.ObjectId(req.params.poolId),
                        matchDate: { $lt: momentTimezone.tz(new Date(), helper.timezone).utc().format('YYYY-MM-DD') }
                    }
                },
                { $lookup: { from: 'matches', localField: 'matchId', foreignField: 'gameId', as: 'match' } },
                { $lookup: { from: 'teams', localField: 'match.homeTeam', foreignField: '_id', as: 'homeTeam' } },
                { $lookup: { from: 'teams', localField: 'match.awayTeam', foreignField: '_id', as: 'awayTeam' } },
                { $sort: { matchDate: -1 } }
            ])
                .exec((error, matchData) => {
                    console.log('matchData', matchData)
                    console.log('error', error)

                    if (error) {
                        return res.status(200).json({
                            title: 'Something Went wrong, please try again.',
                            error: true,
                            detail: error
                        });
                    }
                    if (!matchData) {
                        return res.status(200).json({
                            title: 'No match data found',
                            error: false,
                            detail: matchData
                        });
                    }
                    else {
                        poolMatchData.passed = matchData;
                        callback(null, 'done');
                    }
                })
        }
    ], function (err, result) {
        // result now equals 'done';
        helper.getAvailableTeams(req.params.poolId, req.params.playerId, (error, valueSelected, teamSelected) => {
            console.log('poolMatchData', poolMatchData);
            return res.status(200).json({
                title: 'Match data fetched sucessfully',
                error: false,
                detail: poolMatchData,
                valueSelected: valueSelected,
                teamSelected: teamSelected
            });
        });
    })
}

const getGames = (req, res) => {
    console.log("req.body", req.body)
    let year = new Date().getFullYear();
    let month = new Date().getMonth();
    let query = {};
    helper.getMonthDateRange(year, month, (err, date) => {
        let dateSearch = new Date();
        dateSearch.setHours(0);
        dateSearch.setMinutes(0);
        dateSearch.setSeconds(0);
        console.log("date", momentTimezone.tz(dateSearch, helper.timezone).format("YYYY-MM-DD"))
        if (req.body.userData.role === "user") {
            query = { day: { $gte: momentTimezone.tz(dateSearch, helper.timezone).format("YYYY-MM-DD") } }
        } else {
            query = { day: { $gte: momentTimezone.tz(dateSearch, helper.timezone).format("YYYY-MM-DD") } }
        }
        match.find(query)
            .populate('awayTeam', 'city shortCode teamName')
            .populate('homeTeam', 'city shortCode teamName')
            .populate('winner', 'city shortCode teamName')
            .limit(req.body.skip).skip(req.body.skip * req.body.page)
            .exec((error, games) => {
                if (error) {
                    return res.status(200).json({
                        title: 'Some thing went wrong please try again.',
                        error: true,
                        detail: error
                    });
                } else {
                    console.log('games....', games)
                    let teamSelected = Array(games.length).fill("");
                    console.log('teamSelected', teamSelected);

                    match.find(query)
                        .countDocuments()
                        .exec((err, count) => {
                            if (err) {
                                return res.status(200).json({
                                    title: 'Some thing went wrong please try again.',
                                    error: true,
                                    detail: error
                                });
                            }
                            return res.status(200).json({
                                title: 'Matches fetched sucessfully',
                                error: false,
                                detail: games,
                                selectedTeams: teamSelected,
                                count
                            });
                        })
                }
            });
    });
}

const updateGameDetail = (req, res) => {
    console.log("req.body", req.body);
    // process.exit();
    let update = {}
    if (req.body.winner == '') {
        update = { $set: { homeScore: req.body.homeScore, awayScore: req.body.awayScore, status: req.body.status } }
    } else {
        update = { $set: { homeScore: req.body.homeScore, awayScore: req.body.awayScore, winner: req.body.winner, status: req.body.status } }
    }

    match.findOneAndUpdate({ _id: req.body.id }, update, (error, response) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        }

        console.log("response updateGameDetail", response, error);
        poolPlayerGame.find({ matchId: response.gameId })
            .exec((err, data) => {
                console.log('err, data', err, data);
                if (err) {
                    return res.status(200).json({
                        title: 'Something went wrong please try again.',
                        error: true,
                        detail: error
                    });
                }

                if (req.body.status == 'rescheduled') {
                    console.log("inside rescheduled");
                    /*  if (moment().format('D') !== moment(moment().endOf('month')).format('D')) {
                         var date = new Date()
                         date.setDate(date.getDate() + 1) */
                    /*  poolPlayerGame.updateMany({ matchId: response.gameId }, { $push: { multiPick: { date: date, noOfTeams: 1 } } }).exec((er, playerGame) => {
                         console.log('playerGame', playerGame);

                     }) */

                    async.eachOfSeries(data, (eachData, key, callBack) => {
                        // console.log("eachData", eachData);

                        availableTeams.find({ userId: eachData.playerId })
                            .exec((error1, availableTeam) => {
                                console.log('availableTeamsss', availableTeam);

                                if (error1) {
                                    console.log('error1', error1);
                                }
                                async.eachOfSeries(availableTeam, (eachTeam, k, cb) => {
                                    console.log("eachTeam,k", eachTeam, k);
                                    console.log('eachData.teamSelected', eachData.teamSelected);

                                    let index = eachTeam.selected.findIndex((e) => e.toString() === (eachData.teamSelected).toString())
                                    console.log("indexxxx", index);

                                    if (index > -1) {
                                        availableTeams.updateOne({ userId: eachData.playerId, poolId: eachData.poolId }, { $pull: { selected: eachTeam.selected[index] } }).then((team, err1) => {
                                            console.log("err1", err1);
                                            console.log("team", team);

                                        })

                                        console.log('momennnnt', moment().format('D'), moment(moment().endOf('month')).format('D'));
                                        let date = momentTimezone.tz(moment(), 'America/New_York').format('YYYY-MM-DD')
                                        console.log('momeentt', moment(date).format('D'));
                                        if (/* moment().format('D')  */ moment(date).format('D') !== moment(moment().endOf('month')).format('D')) {
                                            let date1 = moment(response.day, "YYYY-MM-DD").add(1, 'days');
                                            date1 = moment(date1).format('YYYY-MM-DD');
                                            console.log(" momentTimezone.tzzz$$$", date1);
                                            availableTeams.updateOne({ userId: eachData.playerId, poolId: eachData.poolId }, { $push: { multiPick: { noOfTeams: 1, date: date1 } } }).then((team, err1) => {
                                                console.log("err1", err1);
                                                console.log("team", team);
                                            })
                                        } else {
                                            let futureMonth = moment().add(1, 'M');
                                            futureMonth = moment(futureMonth).format('M')
                                            console.log('futureMonth');
                                            match.aggregate([
                                                {
                                                    $match: {
                                                        $or:
                                                            [
                                                                {
                                                                    awayTeam: mongoose.Types.ObjectId(eachData.teamSelected)
                                                                },
                                                                {
                                                                    homeTeam: mongoose.Types.ObjectId(eachData.teamSelected)
                                                                }
                                                            ],
                                                    }
                                                },
                                                {
                                                    $project:
                                                    {
                                                        cmpTo250: { $month: "$day" },
                                                        gameId: "$gameId",
                                                        seasonType: "$seasonType",
                                                        season: "$season",
                                                        awayTeam: "$awayTeam",
                                                        homeTeam: "$homeTeam",
                                                        day: "$day",

                                                        dateTime: "$dateTime"
                                                    }
                                                },
                                                {
                                                    "$match": {
                                                        cmpTo250: parseInt(futureMonth)
                                                    }
                                                },
                                                {
                                                    $sort: { day: 1 }
                                                }
                                            ])
                                                .exec((err, results) => {
                                                    console.log('results@@@^^^', results);
                                                    if (results.length > 0) {
                                                        let tempDate = momentTimezone.tz(results[0].dateTime, helper.timezone).format('YYYY-MM-DD')
                                                        let m = moment(results[0].dateTime);
                                                        m.set({ hour: 12, minute: 0, second: 0, millisecond: 0 })
                                                        m.format()

                                                        poolPlayerGame.findOneAndDelete({ matchId: response.gameId }).exec((error, playerGame) => {
                                                            console.log('deleted error,playerGame', error, playerGame);
                                                        })
                                                        let poolgame = new poolPlayerGame({
                                                            playerId: eachData.playerId,
                                                            matchId: results[0].gameId,
                                                            poolId: eachData.poolId,
                                                            teamSelected: eachData.teamSelected,
                                                            editableUpto: m,
                                                            matchDate: tempDate
                                                        })
                                                        poolgame.save((err, poolGameData) => {
                                                            if (err) {
                                                                console.log("create poolgame addpick", err)
                                                            } else {
                                                                console.log("pooldata Saved", poolGameData);
                                                            }
                                                        });
                                                    }
                                                })
                                            availableTeams.updateOne({ userId: eachData.playerId, poolId: eachData.poolId }, { $push: { selected: eachData.teamSelected } }).then((team, err1) => {
                                                console.log("err1", err1);
                                                console.log("team", team);

                                            })
                                        }
                                        cb();
                                    } else {
                                        cb();
                                    }

                                }, (errorr) => {
                                    if (errorr) {
                                        console.log('errorr in removing selected teams', errorr);
                                    }
                                    console.log('completed removing selected teams');
                                    callBack();
                                })
                            })
                    }, (error) => {
                        if (error) console.log('errorr in async success poolgame ');
                        console.log("done~!");
                    })
                }

                /* } */

                if (!data || (data && data.length == 0)) {
                    return res.status(200).json({
                        title: 'Match details updated sucessfully',
                        error: false,
                        detail: response
                    });
                }
                else {
                    async.eachOfSeries(data, (eachData, key, callback) => {
                        console.log("eachData", (eachData.teamSelected).toString() == req.body.winner.toString())
                        eachData.score = (eachData.teamSelected).toString() == req.body.winner.toString() ? 1 : 0;

                        eachData.save((saveError, savedData) => {
                            console.log("saveError", saveError);
                            console.log("savedData", savedData);
                            if (saveError) {
                                callback();
                            } else {
                                helper.calculateStandings(eachData.poolId);
                                callback();
                            }
                        })
                    }, (error) => {
                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong please try again.',
                                error: true,
                                detail: error
                            });
                        } else {

                            return res.status(200).json({
                                title: 'Match details updated sucessfully',
                                error: false,
                                detail: response
                            });
                        }
                    })
                }
            })
    })
}

const getAvailableTeams = (req, res) => {
    availableTeams.find({
        $and: [
            { userId: req.body.userId },
            { poolId: req.body.poolId },
        ]
    })
        .exec((err, response) => {
            if (err) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error
                });
            } else {
                console.log('getAvailableTeams respnose', response);
                return res.status(200).json({
                    title: 'Teams fetched sucessfully',
                    error: false,
                    detail: response
                });
            }
        })
}

const uploadProfile = (req, res) => {
    var picName = null;
    console.log("req.body.profileimageMain", req.body.profileimageMain == undefined)
    if (req.body.profileimageMain != undefined) {
        picName = randomstring.generate({
            length: 8,
            charset: 'alphanumeric'
        });

        var base64Data = req.body.profileimageMain.replace(/^data:image\/png;base64,/, "");
        helper.createDir(`./uploads`);
        helper.createDir(`./uploads/users`);
        require("fs").writeFile(`./uploads/users/${picName}.png`, base64Data, 'base64', function (err) {
            console.log('err........', err);
        })
    }

    user.findById(req.body.userData._id, (error, user) => {
        console.log("error", error);
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        } else {
            user.dob = req.body.dob;
            user.mobile = req.body.mobile;
            user.name = req.body.name;
            user.username = req.body.username;
            if (req.body.profileimageMain != undefined || req.body.profileimageMain == null) {
                user.profileImage = picName;
            }
            user.save((userError, updatedUser) => {
                if (userError) {
                    return res.status(200).json({
                        title: 'Something went wrong please try again.',
                        error: true,
                        detail: userError
                    });
                } else {
                    if (user.profileImage != null) {
                        picName = 'http://poolsidesports.com/users/' + user.profileImage + '.png';
                    }

                    return res.status(200).json({
                        title: 'Profile details updated sucessfully',
                        error: false,
                        detail: updatedUser,
                        profileImage: picName
                    });
                }
            });
        }
    });
}

const usernameExist = (req, res) => {
    console.log('req.body.username', req);
    user.findOne({ username: req.body.username }, (error, user) => {
        console.log('req.body.user', user)
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: false,
                detail: error
            });
        }
        if (!user) {
            return res.status(200).json({
                title: 'No user found.',
                error: false,
                detail: user
            });
        }
        if (user.length == 0) {
            return res.status(200).json({
                title: 'No user found.',
                error: false,
                detail: user
            });
        }
        else {
            return res.status(200).json({
                title: 'username already exist.',
                error: true,
                detail: user
            });
        }
    })
}

const poolDetail = (req, res) => {
    console.log('req.headers.host', req.headers)
    // return;
    standing.aggregate([
        { $match: { poolId: mongoose.Types.ObjectId(req.params.id), status: true } },
        {
            $group: {
                _id: "$poolId",
                players: { $push: { playerId: '$playerId' } },
            }
        },
        { $lookup: { from: 'pools', localField: '_id', foreignField: '_id', as: 'poolDetails' } },
        { $lookup: { from: 'users', localField: 'players.playerId', foreignField: '_id', as: 'players' } },
        { $lookup: { from: 'users', localField: 'poolDetails.winner', foreignField: '_id', as: 'winner' } }
    ])
        .then((poolDetail, error) => {
            // console.log("poolDetail",poolDetail,error)
            if (error) {
                return res.status(200).json({
                    title: 'Pool details not found.',
                    error: true,
                    detail: error
                });
            }
            if (!poolDetail || (poolDetail && poolDetail.length < 0)) {
                if (error) {
                    return res.status(200).json({
                        title: 'Pool details not found.',
                        error: true,
                        detail: error
                    });
                }
            }
            else {
                return res.status(200).json({
                    title: 'Pool details fetched successfully.',
                    error: false,
                    detail: poolDetail,
                    url: 'http://poolsidesports.com/users'
                });
            }
        })
        .catch((error) => {
            console.log('erororo', error)
            return res.status(200).json({
                title: 'Pool details not found.',
                error: true,
                detail: error
            });
        })
}

const addgameFormat = (req, res) => {
    console.log('body.....', req.body)
    let format = new gameFormat({
        discription: req.body.discription,
        title: req.body.title,
        code: req.body.code,
    });
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        format.save((error, gFormat) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again.',
                    error: true,
                    detail: error
                });
            } else {
                return res.status(200).json({
                    title: 'Game-format created successfully.',
                    error: false,
                    detail: gFormat
                });
            }
        })
    }
}

const getgameFormat = (req, res) => {
    gameFormat.find()
        .exec((error, gFormat) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again.',
                    error: true,
                    detail: error
                });
            } else {
                return res.status(200).json({
                    title: 'Game-format details fetched successfully.',
                    error: false,
                    detail: gFormat
                });
            }
        })
}

const joinPool = (req, res) => {
    standing.find({ poolId: req.body.poolId, status: true })
        .populate('poolId', 'maxPlayers')
        .exec((fetchErr, countData) => {
            console.log("countData", countData)
            if (fetchErr) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again.',
                    error: true,
                    detail: fetchErr
                });
            }
            if ((countData && countData.length > 0) && (countData.length == 1000)) {//parseInt(countData[0].poolId.maxPlayers))) {
                return res.status(200).json({
                    title: 'No more members allowed.',
                    error: true,
                    detail: fetchErr
                });
            } else {
                standing.find({ playerId: req.body.userData._id, poolId: req.body.poolId, status: true })
                    .populate('poolId', 'status maxPlayers owner month')
                    .exec((error, result) => {
                        console.log("error", error);
                        console.log("result", result);
                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong, Please try again.',
                                error: true,
                                detail: error
                            });
                        }
                        if (result && result.length > 0) {
                            return res.status(200).json({
                                title: 'You are already member of this pool.',
                                error: true,
                                detail: error
                            });
                        }
                        else {
                            let newStanding = new standing({
                                playerId: req.body.userData._id,
                                poolId: req.body.poolId
                            })
                            newStanding.save((err, data) => {
                                console.log("newStanding error", err)
                                console.log("newStanding response", data)
                                if (err) {
                                    return res.status(200).json({
                                        title: 'Something went wrong, Please try again.',
                                        error: true,
                                        detail: err
                                    });
                                } else {
                                    const TempAvailableTeams = new availableTeams({
                                        userId: req.body.userData._id,
                                        poolId: req.body.poolId,
                                        selected: [],
                                        selectionForMonth: momentTimezone.tz(new Date(), helper.timezone).format('DD/MM/YYYY')
                                    })
                                    TempAvailableTeams.save((err, data) => {
                                        console.log("error", err);
                                        console.log("data", data);
                                    })
                                    return res.status(200).json({
                                        title: 'You have successfully joined the pool.',
                                        error: false,
                                        detail: data
                                    });
                                }
                            });
                        }
                    })
            }
        });
}

const addPicks = (req, res) => {
    let tempDate = momentTimezone.tz(req.body.gameData.dateTime, helper.timezone).format('YYYY-MM-DD')// moment(req.body.gameData.dateTime).format('YYYY-MM-DD')
    console.log("req....", req.body);
    let teamSelectedId = [];
    async.eachOfSeries((req.body.gameData), (eachGame, key, calBk) => {
        console.log("key.....", key)
        let tempDate = momentTimezone.tz(eachGame.dateTime, helper.timezone).format('YYYY-MM-DD')
        let m = moment(eachGame.dateTime);
        m.set({ hour: 12, minute: 0, second: 0, millisecond: 0 })
        m.format()
        poolPlayerGame.find(
            {
                $and: [
                    { poolId: req.body.poolId },
                    { matchDate: tempDate },
                    { playerId: req.body.playerId }
                ]
            }
        )
            .exec((error, response) => {
                console.log("error", error)
                console.log("response", response, response.length)
                if (error) {
                    return res.status(200).json({
                        title: 'Something went wrong, Please try again.',
                        error: true,
                        detail: error
                    });
                }
                if (!response) {
                    return res.status(200).json({
                        title: 'Something went wrong, Please try again.',
                        error: true,
                        detail: error
                    });
                }
                else {
                    if (response.length > 0 && key == 0) {
                        response.map((value, key) => {
                            teamSelectedId.push(value.teamSelected);
                            value.deleteOne();
                        })
                    }
                    if (req.body.selectedValue[key] || req.body.selectedValue[key] !== undefined) {
                        let poolGame = new poolPlayerGame({
                            playerId: req.body.playerId,
                            matchId: eachGame.gameId,
                            poolId: req.body.poolId,
                            teamSelected: req.body.selectedValue[key],
                            editableUpto: m,
                            matchDate: tempDate
                        });
                        poolGame.save((err, poolGameData) => {
                            if (err) {
                                console.log("create poolgame addpick", err)
                            } else {
                                calBk();
                                console.log("pooldata Saved", poolGame);
                            }
                        });
                    }
                }

            })
    }, (err) => {
        if (err) {
            console.log('err', err);
            return res.status(200).json({
                title: 'Pick saved successfully..',
                error: false,
                detail: []
            });
        }
        availableTeams.find({ userId: req.body.playerId, poolId: req.body.poolId })
            .exec((error, availTeam) => {
                if (error) {
                    console.log("eror", error);
                }
                if (availTeam && availTeam.length === 0) {
                    console.log("no available teams found.")
                } else {
                    console.log("availableTeams", availTeam);
                    let combineTeamId = availTeam[0].selected.concat(req.body.selectedValue);

                    let combineTeamIdArray = combineTeamId.map((val, key) => {
                        console.log("val", val)
                        if (val != '' && teamSelectedId.findIndex((e) => e.toString() == val.toString()) == -1) {
                            return val;
                        }
                        return null;
                    }).filter(v => {
                        console.log("val != null", v)
                        return v != null;
                    });

                    console.log('combineTeamId', combineTeamId);
                    console.log('combineTeamIdArray', combineTeamIdArray);
                    console.log('teamSelectedId', teamSelectedId);

                    availTeam[0].selected = combineTeamIdArray;
                    availTeam[0].save((err, saveData) => {
                        console.log(err)
                        console.log("saveData", saveData)
                    });

                }
            })
        return res.status(200).json({
            title: 'Pick saved successfully..',
            error: false,
            detail: []
        });
    })

}

const getPoolByInviteCode = (req, res) => {
    pool.find({ inviteCode: req.body.inviteCode }, (error, poolDetail) => {
        console.log('poolDetail', poolDetail, error);



        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        }
        if (!poolDetail || poolDetail == 'undefined') {
            return res.status(200).json({
                title: 'No pool found.',
                error: true,
                detail: poolDetail
            });
        } else if (poolDetail.length == 0) {

            return res.status(200).json({
                title: 'No pool found.',
                error: true,
                detail: poolDetail
            });
        }
        else {
            let date = new Date();
            let poolMonth = moment(poolDetail[0].month, 'YYYY/MM/DD');
            // let poolMonth = new Date(poolData.month);
            console.log('date', date.getMonth(), typeof date.getMonth());
            console.log('poolMonth', poolMonth.format('M'), typeof poolMonth.format('M'));
            console.log('date.getMonth().toString() == poolMonth.toString()', date.getMonth().toString(), poolMonth.format('M'), date.getMonth().toString() == poolMonth.format('M').toString());
            if (date.getMonth().toString() == poolMonth.format('M')) {
                console.log('inside equal');

                return res.status(200).json({
                    title: 'Pool joining date has expired.',
                    error: true,
                    detail: poolDetail
                });
            } else {
                return res.status(200).json({
                    title: 'Pool fetched successully.',
                    error: false,
                    detail: poolDetail
                });
            }

        }
    })
}
3
const getGameSearch = (req, res) => {
    console.log("req.body.............", req.body, req._startTime)
    let searchDate = momentTimezone.tz(req.body.fromDate, req.body.timeZone).format("YYYY-MM-DD");
    let currentDate = momentTimezone.tz(new Date(), helper.timezone).utc();
    // let searchDate = new Date(req.body.fromDate);
    // var m = moment(req.body.fromDate);
    // m.set({ hour: 12, minute: 0, second: 0, millisecond: 0 })
    let setDate = (momentTimezone.tz(new Date(new Date(req.body.fromDate).getTime() + (1 * 12 * 60 * 60000)), req.body.timeZone).utc())

    // searchDate.setMinutes(0);
    // searchDate.setSeconds(0);
    // searchDate = momentTimezone(new Date(searchDate)).format("YYYY-MM-DD")
    let nextDate;
    let dateSearch;
    if (req.body.fromDate == req.body.toDate || req.body.toDate == "") {
        let newDate = momentTimezone.tz(req.body.fromDate, req.body.timeZone) //new Date(req.body.toDate);
        console.log('newDate', new Date(newDate))
        nextDate = (momentTimezone.tz(new Date(new Date(newDate).getTime() + (1 * 24 * 60 * 60000)), req.body.timeZone).format("YYYY-MM-DD"));
    } else {
        console.log("asdasdad")
        let newDate = new Date(req.body.toDate);
        newDate.setHours(0);
        newDate.setMinutes(0);
        newDate.setSeconds(0);
        nextDate = (momentTimezone.tz(new Date(newDate.getTime() + (1 * 24 * 60 * 60000)), req.body.timeZone).format("YYYY-MM-DD"))
    }

    let currentTime = momentTimezone.tz(new Date(nextDate), 'America/New_York').utc() >= currentDate && currentDate >= setDate;

    console.log("rdate", searchDate)
    console.log("nextDate", nextDate, setDate)
    console.log("searchDate", currentDate, momentTimezone.tz(new Date(nextDate), 'America/New_York').utc());
    console.log("consition", (momentTimezone.tz(new Date(nextDate), 'America/New_York').utc() >= currentDate) && currentDate >= setDate)
    let query = {
        $and: [
            { day: { $gte: searchDate } },
            { day: { $lt: nextDate } },
        ]
    };
    console.log("query", query)
    match.find(query)
        .populate('awayTeam', 'city shortCode teamName')
        .populate('homeTeam', 'city shortCode teamName')
        .populate('winner', 'city shortCode teamName')
        .limit(parseInt(req.body.skip)).skip(parseInt(req.body.skip) * parseInt(req.body.page))
        .exec((error, games) => {
            console.log("error", error)
            if (error) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error,
                    currentTime
                });
            } else {
                let teamSelected = Array(games.length).fill("");
                console.log('teamSelected', teamSelected);

                match.find(query)
                    .countDocuments()
                    .exec((err, count) => {
                        if (err) {
                            return res.status(200).json({
                                title: 'Some thing went wrong please try again.',
                                error: true,
                                detail: error,
                                currentTime
                            });
                        }
                        return res.status(200).json({
                            title: 'Matches fetched sucessfully',
                            error: false,
                            detail: games,
                            fromDate: req.body.fromDate,
                            toDate: req.body.toDate,
                            selectedTeams: teamSelected,
                            count,
                            currentTime
                        });
                    })
            }
        });
}

const changeUserStatus = (req, res) => {
    user.findOneAndUpdate({ _id: req.body.userId }, { $set: { isActive: req.body.isActive } }, (err, response) => {
        if (err) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        }
        return res.status(200).json({
            title: 'User status changed successfully',
            error: false,
            detail: response,
        });
    })
}

const updatePoolDetail = (req, res) => {
    console.log("req.body", req.body)
    pool.findByIdAndUpdate({ _id: req.body.poolId }, { $set: { multiPick: req.body.multiPick, status: req.body.status, winner: req.body.winner !== '' ? req.body.winner : null, tiedPlayers: req.body.tiedPlayers !== '' ? req.body.tiedPlayers : null } }, (error, updatedValue) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong.',
                error: false,
                detail: updatedValue
            });
        }
        else {
            return res.status(200).json({
                title: 'Pool details updated successfully.',
                error: false,
                detail: updatedValue
            });
        }
    })
}

const addInterest = (req, res) => {
    let interestArray = [];
    req.body.interest.map((val, key) => {
        if (val.interestId !== '') {
            return interestArray.push(val.interestId)
        }
    }).filter((e) => (e != undefined || e != ''))
    user.findByIdAndUpdate((req.body.userData._id), { $set: { interest: interestArray } }, (error, userUpdatedData) => {
        console.log(error, userUpdatedData)
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong.',
                error: true,
                detail: userUpdatedData
            });
        }
        else {
            if (req.body.action == "delete") {
                return res.status(200).json({
                    title: 'Interest removed successfully.',
                    error: false,
                    detail: userUpdatedData
                });
            } else {
                return res.status(200).json({
                    title: 'Interest added successfully.',
                    error: false,
                    detail: userUpdatedData
                });
            }
        }
    })
}

const fetchLeaderboardData = (req, res) => {
    console.log("req!!!s", req.body, req.params);
    let query = { poolId: mongoose.Types.ObjectId(req.body.data.poolId), status: true }
    if (req.body.userData.role == 'admin') {
        query = { poolId: mongoose.Types.ObjectId(req.body.data.poolId) }
    }
    standing.find(query)
        .populate('playerId')
        .populate('poolId')
        .sort({ 'wins': -1 }).exec((error, standings) => {
            console.log('fetchLeaderboardData standings', standings)
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong please try again.',
                    error: false,
                    detail: error
                });
            }
            if (!standings) {
                return res.status(200).json({
                    title: 'No standings found.',
                    error: false,
                    detail: standings
                });
            }
            if (standings.length == 0) {
                return res.status(200).json({
                    title: 'No standings found.',
                    error: false,
                    detail: standings
                });
            }
            else {
                return res.status(200).json({
                    title: 'Standings found.',
                    error: false,
                    detail: standings
                });
            }
        })

}

const matchDetail = (req, res) => {
    standing
        .findOne({ playerId: mongoose.Types.ObjectId(req.params.playerId), poolId: mongoose.Types.ObjectId(req.params.poolId) }).populate('playerId').populate('poolId')
        .exec((error, standing) => {

            poolPlayerGame.aggregate([
                {
                    $match: { playerId: mongoose.Types.ObjectId(req.params.playerId), poolId: mongoose.Types.ObjectId(req.params.poolId) }
                },
                { $lookup: { from: 'matches', localField: 'matchId', foreignField: 'gameId', as: 'match' } },
                { $lookup: { from: 'teams', localField: 'match.homeTeam', foreignField: '_id', as: 'homeTeam' } },
                { $lookup: { from: 'teams', localField: 'match.awayTeam', foreignField: '_id', as: 'awayTeam' } },
                {
                    $sort: {
                        matchDate: -1
                    }
                }
            ])
                // .sort({matchDate: -1})
                .exec((error1, games) => {
                    console.log("error1,games", error1, games);

                    return res.status(200).json({
                        title: 'User found.',
                        error: false,
                        detail: standing,
                        games: games,
                        url: 'http://poolsidesports.com/users/'

                    });
                })

        })
}

const removeUser = (req, res) => {
    console.log("req.body", req.body.data);
    let title = 'User removed successfully.'
    if (req.body.data.status == true) {
        title = 'User activated successfully.'
    }
    standing.findOneAndUpdate({ poolId: req.body.data.poolId, playerId: req.body.data.playerId }, { $set: { status: req.body.data.status } }).exec((error, standings) => {
        console.log("standingss", standings);

        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        }
        if (!standings) {
            return res.status(200).json({
                title: 'No user found.',
                error: true,
                detail: standings
            });
        }
        else {
            return res.status(200).json({
                title: title,
                error: false,
                detail: standings
            });
        }
    })
}

const poolnameExist = (req, res) => {
    console.log('req.body.poolName', req.body.poolName);
    pool.findOne({ poolName: req.body.poolName }, (error, pool) => {
        console.log('pool', pool)
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: false,
                detail: error
            });
        }
        if (!pool) {
            return res.status(200).json({
                title: 'No pool found.',
                error: false,
                detail: pool
            });
        }
        if (pool.length == 0) {
            return res.status(200).json({
                title: 'No pool found.',
                error: false,
                detail: pool
            });
        }
        else {
            return res.status(200).json({
                title: 'Poolname is already taken.',
                error: true,
                detail: pool
            });
        }
    })
}

var job = new CronJob({
    // cronTime: '30 * * * * *',
    cronTime: '59 59 11 * * *',
    // cronTime: '00 58 05 * * *',
    onTick: function (next) {
        user
            .find({ isBlocked: 0, role: 'user' })
            .exec((error, users) => {
                if (error) {
                    console.log("error user", users)
                }
                if (!user) {
                    console.log("! user", users)
                } else {
                    allocateTeam(users, (res) => {
                        console.log("completed wit cron", res)
                    })
                }
            })
    },
    start: true,
    timeZone: helper.timezone
});

const allocateTeam = (users, cb) => {
    async.eachOfSeries(users, (user, key, innerCallback) => {
        console.log("user", key, user)
        var m = moment() //moment('2019-07-18 12:00:00.000Z');
        m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
        console.log("hiiii", momentTimezone.tz(new Date(), helper.timezone).day(), momentTimezone.tz(new Date(m), helper.timezone).format());
        let date = momentTimezone.tz(new Date(m), helper.timezone).format();
        let day = momentTimezone.tz(new Date(m), helper.timezone).day();
        let lastDateOfMonth = moment().endOf('month');//.format('YYYY-MM-DD');
        console.log('day.....', day, date, momentTimezone.tz(new Date(m), helper.timezone).utc())
        let tommorrow = moment(date).add(1, 'days');
        let dayAfterTommorrow = moment(tommorrow).add(1, 'days');
        let weekEndsDateArray = [date]
        if (tommorrow <= lastDateOfMonth) {
            weekEndsDateArray.push(momentTimezone.tz(moment(date).add(1, 'days').format(), helper.timezone).format())
        }
        if (dayAfterTommorrow <= lastDateOfMonth) {
            weekEndsDateArray.push(momentTimezone.tz(moment(new Date(tommorrow)).add(1, 'days').format(), helper.timezone).format())
        }

        async.waterfall([
            function (callback) {
                team
                    .find()
                    .sort({ teamName: 1 })
                    .exec((error, teams) => {
                        if (error) {
                            console.log("teams error", error);
                            callback(null, 'done');
                        } else {
                            teams = teams.sort(() => Math.random() - 0.5);
                            callback(null, teams);
                        }
                    });
            },
            function (teams, callback) {
                /*   if (day >= 5) {
                      console.log('weekEndsDateArray', weekEndsDateArray)
                      async.eachOfSeries(weekEndsDateArray, (date, key, weekEndCB) => {
                          console.log(" weekEndsDateArray date", momentTimezone.tz(new Date(date), helper.timezone).format("YYYY-MM-DD"))
                          match
                              .find({ day: date })
                              .exec((error, matches1) => {
                                  if (error) {
                                      console.log("weekEndsDateArray matches error", error);
                                      callback(null, 'done');
                                  } else {
                                      pickAllotment(user, matches1, teams, new Date(date), (callback => {
                                          console.log("weekEndsDateArray weekEndCB")
                                          weekEndCB()
                                      }));
                                  }
                              });
                      }, (err, res) => {
                          callback(null);
                      }); */
                // } else {
                // console.log("weekdays")
                match
                    .find({ day: date })
                    .exec((error, matches) => {
                        if (error) {
                            console.log("matches error", error);
                            callback(null, 'done');
                        } else {
                            console.log("matches", matches, date);
                            pickAllotment(user, matches, teams, new Date(m), callback);
                        }
                    })
                // }
            }
        ], function (err, result) {
            // result now equals 'done'
            innerCallback(null)
        });
    }, (err, res) => {
        if (err) {
            console.log(err)
        } else {
            cb("compeletd")
        }
    })
}

pickAllotment = (user, matches, teams, gameDate, callback) => {
    availableTeams
        .find({ userId: user._id })
        .populate('poolId')
        .exec((err, availableTeam) => {
            if (err) {
                console.log('err', err);
                return callback(null, 'done');
            }
            if (!availableTeam || availableTeam.length == 0) {
                console.log("no available teams");
                return callback(null, 'done');
            } else {
                console.log("availableTeam.......", availableTeam);
                async.eachOfSeries(availableTeam, (eachAvailData, key1, cbAvailableTeam) => {
                    console.log('eachAvailData##', eachAvailData.selected);

                    let count = 0;
                    async.eachOfSeries(teams, (team, key, cbTeam) => {

                        poolPlayerGame.find({
                            $and: [
                                { playerId: eachAvailData.userId, poolId: eachAvailData.poolId._id, matchDate: momentTimezone.tz(gameDate, helper.timezone).utc().format("YYYY-MM-DD") }
                            ]
                        })

                            .exec((error, poolGame) => {
                                let month = eachAvailData.poolId.month.getMonth();
                                console.log('gameDate@@ month compare$$$', gameDate, month, gameDate.getMonth(), gameDate.getMonth() == month);

                                console.log("poolGame", poolGame, eachAvailData.poolId.multiPick);
                                console.log("eachAvailData.selected.length ", eachAvailData.selected.length);

                                if (error) {
                                    console.log("create poolgame addpick.....", error)
                                    return cbTeam(null, 'done');
                                }
                                if (gameDate.getMonth() == month) {
                                    if (poolGame && poolGame != undefined) {
                                        let index;
                                        let matchIndex = 0;
                                        let multiPickCount = 1;
                                        let multiPickIndex = eachAvailData.poolId.multiPick.findIndex((e) => momentTimezone.tz(e.date, helper.timezone).utc().format("YYYY-MM-DD") === momentTimezone.tz(gameDate, helper.timezone).utc().format("YYYY-MM-DD"))

                                        let noPickDaysIndex = eachAvailData.poolId.noPickDays.findIndex((e) => momentTimezone.tz(e, helper.timezone).utc().format("YYYY-MM-DD") === momentTimezone.tz(gameDate, helper.timezone).utc().format("YYYY-MM-DD"))

                                        console.log("multiPickIndex", multiPickIndex, noPickDaysIndex, multiPickCount)
                                        if (eachAvailData.selected.length == 0) {
                                            console.log('else conditiom', team._id)
                                            index = -1;
                                        } else {
                                            console.log('team._id', team._id)
                                            index = eachAvailData.selected.findIndex((e) => e.toString() == team._id.toString());
                                            console.log('index', index);
                                        }

                                        if (noPickDaysIndex > -1) {
                                            return cbTeam(null, 'done');
                                        }
                                        if (multiPickIndex > -1) {
                                            multiPickCount = parseInt(eachAvailData.poolId.multiPick[multiPickIndex].noOfTeams);
                                        }


                                        if (poolGame && poolGame.length > 0) {
                                            console.log("key", key);
                                            // matches.map((e, k) => {
                                            matchIndex = -1;
                                            for (var j = 0; j < matches.length; j++) {
                                                if (matchIndex > 0) {
                                                    break;
                                                }
                                                for (var i = 0; i < poolGame.length; i++) {
                                                    console.log("poolgame.matchId", poolGame[i].matchId, matches[j].gameId, poolGame[i].matchId.toString() != matches[j].gameId.toString());
                                                    if (poolGame[i].matchId.toString() != matches[j].gameId.toString()) {
                                                        let newIndex = matches.findIndex((e) => e.awayTeam.toString() == team._id.toString() || e.homeTeam.toString() == team._id.toString())
                                                        console.log("newIndex", newIndex)
                                                        if (newIndex > -1 && matches[newIndex].gameId.toString() != poolGame[i].matchId.toString()) {
                                                            matchIndex = newIndex;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            // });

                                            console.log('matchIndex if....', matchIndex)
                                            if (index < 0 && matchIndex > -1 && multiPickCount > poolGame.length) {
                                                count++;
                                                let poolGame = new poolPlayerGame({
                                                    playerId: user._id,
                                                    matchId: matches[matchIndex].gameId,
                                                    poolId: eachAvailData.poolId,
                                                    teamSelected: team._id,
                                                    editableUpto: gameDate,
                                                    matchDate: momentTimezone.tz(gameDate, helper.timezone).utc().format("YYYY-MM-DD")
                                                });
                                                poolGame.save((err, poolGameData) => {
                                                    if (err) {
                                                        console.log("create poolgame addpick222222", err)
                                                        return cbAvailableTeam(null, 'done');
                                                    } else {
                                                        console.log("pooldata Saved");
                                                        availableTeams.updateOne({ userId: user._id, poolId: eachAvailData.poolId }, { $push: { selected: team._id } }).then((availteamSave, err1) => {
                                                            // console.log("err1", err1);
                                                            console.log("count", count, multiPickCount);
                                                            if (count == multiPickCount) {
                                                                return cbAvailableTeam()
                                                            } else {
                                                                return cbTeam(null, 'done');
                                                            }
                                                        })
                                                    }
                                                });
                                            } else {
                                                return cbTeam(null, 'done');
                                            }

                                        } else {
                                            matchIndex = matches.findIndex((e) => e.awayTeam.toString() == team._id.toString() || e.homeTeam.toString() == team._id.toString())

                                            console.log('matchIndex....', matches, team._id, matchIndex, index, multiPickCount)

                                            if (index < 0 && matchIndex > -1 && multiPickCount > poolGame.length) {
                                                count++;
                                                let poolGame = new poolPlayerGame({
                                                    playerId: user._id,
                                                    matchId: matches[matchIndex].gameId,
                                                    poolId: eachAvailData.poolId,
                                                    teamSelected: team._id,
                                                    editableUpto: gameDate,
                                                    matchDate: momentTimezone.tz(gameDate, helper.timezone).utc().format("YYYY-MM-DD")
                                                });
                                                poolGame.save((err, poolGameData) => {
                                                    if (err) {
                                                        console.log("create poolgame addpick222222", err)
                                                        return cbAvailableTeam(null, 'done');
                                                    } else {
                                                        console.log("pooldata Saved");
                                                        availableTeams.updateOne({ userId: user._id, poolId: eachAvailData.poolId }, { $push: { selected: team._id } }).then((availteamSave, err1) => {
                                                            // console.log("err1", err1);
                                                            console.log("count", count, multiPickCount);
                                                            if (count == multiPickCount) {
                                                                return cbAvailableTeam()
                                                            } else {
                                                                return cbTeam(null, 'done');
                                                            }
                                                        })
                                                    }
                                                });
                                            } else {
                                                return cbTeam(null, 'done');
                                            }
                                        }

                                    } else {
                                        return cbTeam(null, 'done');
                                    }
                                } else {
                                    return cbTeam(null, 'done');
                                }

                            });
                    }, (err, response) => {
                        if (err) {
                            return cbAvailableTeam(null, 'done');
                        } else {
                            return cbAvailableTeam(null, 'done');
                        }
                    })
                }, (err, response2) => {
                    if (err) {
                        return callback(null, 'done');
                    } else {
                        return callback(null, 'done');
                    }
                })

            }
        });
}

const addMessage = (req, res) => {
    console.log('add message req.body ', req.body);

    let chatData = new chat({
        message: req.body.message,
        name: req.body.name,
        userId: mongoose.Types.ObjectId(req.body.userId),
        channelId: mongoose.Types.ObjectId(req.body.channelId)
    });

    chatData.save((error, chatSave) => {
        console.log("create chat error", error)
        if (error) {
            console.log("create chat error", error)
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        } else {
            console.log("chatdata Saved", chatSave);
            return res.status(200).json({
                title: 'Chat added successfully.',
                error: false,
                detail: chatSave
            });
        }
    });
}

const addGameDetail = (req, res) => {
    console.log('req.body ~~~~', req.body);
    let day = new Date(momentTimezone.tz(new Date(req.body.dateTime), helper.timezone))
    day.setHours(0);
    day.setMinutes(0);
    day.setSeconds(0);
    day.setMilliseconds(0)
    let game = new match({
        gameId: req.body.gameId,
        awayTeam: req.body.awayTeam,
        homeTeam: req.body.homeTeam,
        dateTime: new Date(momentTimezone.tz(new Date(req.body.dateTime), helper.timezone)),
        day: day
    })
    game.save((error, gameData) => {
        if (error) {
            console.log("create chat error", error)
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        } else {
            console.log("gameData Saved", gameData);
            return res.status(200).json({
                title: 'Game added successfully.',
                error: false,
                detail: gameData
            });
        }
    })
}

const deleteGameDetail = (req, res) => {
    console.log('req.body deleteGameDetail', req.body);
    match.findByIdAndRemove({ _id: req.body.id }).exec((error, matchData) => {
        if (error) {
            console.log("error in deleting", error)
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        } else {
            console.log("matchData deleted", matchData);
            return res.status(200).json({
                title: 'Match deleted successfully.',
                error: false,
                detail: matchData
            });
        }
    })
}

const gameIdExist = (req, res) => {
    console.log('req.body.gameId', req.body);
    match.findOne({ gameId: req.body.gameId }, (error, matchData) => {
        console.log('matchData', matchData)
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: false,
                detail: error
            });
        }
        if (!matchData) {
            return res.status(200).json({
                title: 'No gameId found.',
                error: false,
                detail: matchData
            });
        }
        if (matchData.length == 0) {
            return res.status(200).json({
                title: 'No gameId found.',
                error: false,
                detail: matchData
            });
        }
        else {
            return res.status(200).json({
                title: 'Game Id already exist.',
                error: true,
                detail: matchData
            });
        }
    })
}

const updateGames = (req, res) => {
    console.log("req.body", req.body);
    async.eachOfSeries(req.body.selectedTeams, (eachTeam, ind, cb) => {
        if (eachTeam == undefined || eachTeam == null) {
            cb();
        } else {
            match.findOneAndUpdate({ _id: eachTeam._id }, { $set: { homeScore: '', awayScore: '', status: eachTeam.status, winner: eachTeam.winner } }, (error, response) => {
                if (error) {
                    return cb()
                }

                console.log("response updateGames", response, error);
                poolPlayerGame.find({ matchId: eachTeam.gameId })
                    .exec((err, data) => {
                        console.log('err, data', err, data);
                        if (err) {
                            console.log('errrrrrrrr', err);
                            cb()
                        }

                        if (!data || (data && data.length == 0)) {
                            console.log("data", data.length);
                            cb()
                        }
                        else {
                            async.eachOfSeries(data, (eachData, key, cback) => {
                                // console.log("eachTeam.winner.valueOf()",eachTeam.winner.valueOf())
                                eachData.score = eachTeam.winner && eachTeam.winner != null ?
                                    (eachData.teamSelected.toString() == eachTeam.winner.toString() || (eachTeam.winner.valueOf()._id != undefined && eachData.teamSelected.toString() == eachTeam.winner.valueOf()._id.toString())) ? 1 : 0 : -1;
                                console.log("eachData.score", eachData.score);

                                eachData.save((saveError, savedData) => {
                                    console.log("saveError", saveError);
                                    console.log("savedData", savedData);
                                    if (saveError) {
                                        cback();
                                    } else {
                                        cback();
                                    }
                                })
                            }, (err) => {
                                if (err) {
                                    cb()
                                } else {
                                    cb()
                                }
                            })
                        }
                    })
            })
        }
    }, (error1) => {
        helper.calculateStandings();
        if (error1) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error1
            });
        } else {
            return res.status(200).json({
                title: 'Match details updated sucessfully',
                error: false,
                detail: []
            });
        }
    })
}

const getMessages = (req, res) => {
    console.log('poolid', req.params.poolid);
    chat.find({ channelId: req.params.poolid })
        .countDocuments()
        .exec((err, count) => {
            let tempCount = count < 100 ? 0 : count - 100
            chat.find({ channelId: req.params.poolid })
                .limit(100)
                .skip(tempCount)
                .populate('userId')
                .exec((error, chats) => {
                    pool.findOne({ _id: req.params.poolid }).exec((err, poolData) => {
                        console.log('error', error);
                        console.log('chats%%%%', chats);
                        console.log('poolData^^^', poolData);

                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong please try again.',
                                error: false,
                                detail: error
                            });
                        }
                        if (!chats) {
                            return res.status(200).json({
                                title: 'No chats found.',
                                error: false,
                                detail: matchData
                            });
                        }
                        if (chats.length == 0) {
                            return res.status(200).json({
                                title: 'No chats found.',
                                error: false,
                                detail: chats,
                                poolDetail: poolData
                            });
                        }
                        else {
                            return res.status(200).json({
                                title: 'Chats found',
                                error: true,
                                detail: chats,
                                poolDetail: poolData
                            });
                        }
                    })
                })
        });
}

const removePick = (req, res) => {
    console.log('reqqq', req.body);
    poolPlayerGame.findByIdAndRemove({ playerId: mongoose.Types.ObjectId(req.body.playerId), poolId: mongoose.Types.ObjectId(req.body.poolId), _id: mongoose.Types.ObjectId(req.body.matchId) }).exec((error, game) => {
        console.log('error', error);
        console.log('game', game);

        availableTeams.findOne({ userId: mongoose.Types.ObjectId(req.body.playerId), poolId: mongoose.Types.ObjectId(req.body.poolId) }).exec((err, teams) => {
            console.log('teams', teams);
            let index = -1;
            if (teams && teams != null) {
                index = teams.selected.findIndex((e) => e.toString() === (game.teamSelected).toString())
            }
            console.log("indexxxx", index);

            if (index > -1) {
                availableTeams.updateOne({ userId: mongoose.Types.ObjectId(req.body.playerId), poolId: mongoose.Types.ObjectId(req.body.poolId) }, { $pull: { selected: teams.selected[index] } }).then((team, err1) => {
                    console.log("err1", err1);
                    console.log("team", team);
                })
            }
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong please try again.',
                    error: true,
                    detail: error
                });
            }
            if (!game) {
                return res.status(200).json({
                    title: 'No pick found.',
                    error: true,
                    detail: game
                });
            }
            else {
                return res.status(200).json({
                    title: 'Pick deleted successfully.',
                    error: false,
                    detail: game
                });
            }
        })


    })
}

const getMatchDetails = (req, res) => {
    var teamsArray = [];
    var matchesArr = []
    async.eachOfSeries(req.body.selected, (eachTeam, i, cb) => {
        console.log('dsfkjdjf', eachTeam);

        poolPlayerGame.findOne({ teamSelected: eachTeam }).populate('teamSelected').exec((error, team) => {
            console.log("Teams", team)
            if (team) {
                match.findOne({ gameId: team.matchId }).populate('awayTeam').populate('homeTeam').exec((errr, matches) => {
                    console.log('error,teamssss', error, team);

                    teamsArray.push(team);
                    matchesArr.push(matches)
                    cb()
                })
            } else {
                cb()
            }
        })

    }, (error) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong please try again.',
                error: true,
                detail: error
            });
        } else {
            console.log('teamsArrayssss', teamsArray);
            return res.status(200).json({
                title: 'Games found successfully.',
                error: false,
                detail: teamsArray,
                matches: matchesArr
            });
        }


    })
    // match.
}


module.exports = {
    signup,
    editProfile,
    forgotPassword,
    resetToken,
    changePassword,
    createPool,
    getPools,
    getAllUser,
    getMlbTeams,
    getMlbGames,
    getTeams,
    getProfile,
    getMonthlyMatches,
    getGames,
    updateGameDetail,
    uploadProfile,
    usernameExist,
    poolDetail,
    addgameFormat,
    getgameFormat,
    joinPool,
    addPicks,
    getPoolByInviteCode,
    getGameSearch,
    changeUserStatus,
    updatePoolDetail,
    addInterest,
    fetchLeaderboardData,
    matchDetail,
    removeUser,
    poolnameExist,
    addMessage,
    addGameDetail,
    deleteGameDetail,
    gameIdExist,
    updateGames,
    getMessages,
    removePick,
    getMatchDetails,
    getAvailableTeams
}