var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var userController = require('../controllers/user');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");

var expressValidator = require('express-validator');
router.use(expressValidator())

/* router.get('/', function(req, res, next) {
  res.render('user/addUserForm', { user: user.schema.obj });
}); */

router.post('/signup', (req, res, next) => {
  req.checkBody('username', 'Usernameame is required').notEmpty();
  req.checkBody('name', 'Name is required').notEmpty();
  req.checkBody('email', 'Email is required').notEmpty();
  req.checkBody('email', 'Email is not valid').isEmail();
  req.checkBody('password', 'Password is required').notEmpty();
  // req.checkBody('mobile', 'mobile is required').notEmpty();
  userController.signup(req, res, [])
});

router.post('/login', (req, res, next) => {
  user.findOne({ $or: [{ email: req.body.email }, { username: req.body.email }] })
    .exec((err, userData) => {
      if (err) {
        return res.status(200).json({
          title: "Error occured",
          error: true,
          details: err
        })
      }
      if (!userData) {
        return res.status(200).json({
          title: "user not found",
          error: true,
          details: userData
        });
      }
      if (!bcrypt.compareSync(req.body.password, userData.password)) {
        return res.status(200).json({
          title: 'Invalid password',
          error: true,
          detail: 'password does not match'
        });
      } else {
        var token = jwt.sign({
          email: userData.email,
          userId: userData.id,
          role: userData.role,
          exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60 * 1000),
        }, "poolGame_19_04_2019_");
        return res.status(200).json({
          title: 'Login successful.',
          error: false,
          token: token,
          userData: userData
        });
      }
    });
});

router.post('/forgotPassword', (req, res, next) => {
  userController.forgotPassword(req, res);
});

router.post('/resetToken', (req, res, next) => {
  userController.resetToken(req, res);
});

router.post('/changePassword', [auth.authenticateUser, auth.isAuthenticated('changePassword')], (req, res, next) => {
  userController.changePassword(req, res);
});

router.post('/editUser', [auth.authenticateUser, auth.isAuthenticated('editUser')], (req, res, next) => {
  userController.editProfile(req, res);
})

router.post('/createPool', [auth.authenticateUser, auth.isAuthenticated('createPool')], (req, res, next) => {
  req.checkBody('poolName', 'Pool name is required').notEmpty();
  // req.checkBody('month', 'Month is required').notEmpty();
  // req.checkBody('maxPlayers', 'Max Player is required').notEmpty();
  userController.createPool(req, res, []);
})

router.post('/updatePoolDetail', [auth.authenticateUser, auth.isAuthenticated('updatePoolDetail')], (req, res, next) => {
  userController.updatePoolDetail(req, res)
});

router.get('/getAllUsers', [auth.authenticateUser, auth.isAuthenticated('getAllUsers')], (req, res, next) => {
  userController.getAllUser(req, res)
});

router.get('/getAllPools', [auth.authenticateUser, auth.isAuthenticated('getAllPools')], (req, res, next) => {
  userController.getPools(req, res)
});

router.get('/getMlbTeams', [auth.authenticateUser, auth.isAuthenticated('getMlbTeams')], (req, res, next) => {
  userController.getMlbTeams(req, res)
});

router.get('/getMlbGames', [auth.authenticateUser, auth.isAuthenticated('getMlbTeams')], (req, res, next) => {
  userController.getMlbGames(req, res)
});

router.get('/getTeams', [auth.authenticateUser, auth.isAuthenticated('getTeams')], (req, res, next) => {
  userController.getTeams(req, res)
});

router.post('/getGames', [auth.authenticateUser, auth.isAuthenticated('getGames')], (req, res, next) => {
  userController.getGames(req, res)
});


router.get('/profile', [auth.authenticateUser, auth.isAuthenticated('profile')], (req, res, next) => {
  userController.getProfile(req, res)
});

router.get('/getMonthlyMatches/:poolId/:playerId', [auth.authenticateUser, auth.isAuthenticated('getMonthlyMatches')], (req, res, next) => {
  userController.getMonthlyMatches(req, res)
});

router.post('/updateGameDetail', [auth.authenticateUser, auth.isAuthenticated('updateGameDetail')], (req, res, next) => {
  userController.updateGameDetail(req, res)
});

router.post('/uploadProfile', [auth.authenticateUser, auth.isAuthenticated('uploadProfile')], (req, res, next) => {
  userController.uploadProfile(req, res)
});

router.post('/usernameExist', (req, res, next) => {
  userController.usernameExist(req, res)
});

router.get('/poolDetail/:id', [auth.authenticateUser, auth.isAuthenticated('poolDetail')], (req, res, next) => {
  userController.poolDetail(req, res)
});

router.post('/addgameFormat', [auth.authenticateUser, auth.isAuthenticated('addgameFormat')], (req, res, next) => {
  req.checkBody('discription', 'Discription is required').notEmpty();
  req.checkBody('title', 'Discription is required').notEmpty();
  req.checkBody('code', 'Discription is required').notEmpty();
  userController.addgameFormat(req, res)
});

router.get('/getgameFormat', [auth.authenticateUser, auth.isAuthenticated('getgameFormat')], (req, res, next) => {
  userController.getgameFormat(req, res)
});

router.post('/joinPool', [auth.authenticateUser, auth.isAuthenticated('joinPool')], (req, res, next) => {
  userController.joinPool(req, res)
});

router.post('/addPicks', [auth.authenticateUser, auth.isAuthenticated('addPicks')], (req, res, next) => {
  userController.addPicks(req, res)
});

router.post('/getPoolByInviteCode', (req, res, next) => {
  userController.getPoolByInviteCode(req, res)
});

/* router.get('/getGameSearch/:fromDate/:toDate/:skip/:page', (req, res, next) => {
  userController.getGameSearch(req, res)
});
 */

router.post('/getAvailableTeams', (req, res, next) => {
  userController.getAvailableTeams(req, res);
})

router.post('/getGameSearch', (req, res, next) => {
  userController.getGameSearch(req, res)
});


router.post('/changeUserStatus', [auth.authenticateUser, auth.isAuthenticated('changeUserStatus')], (req, res, next) => {
  userController.changeUserStatus(req, res)
});

router.post('/addInterest', [auth.authenticateUser, auth.isAuthenticated('addInterest')], (req, res, next) => {
  console.log("req.body.interest", req.body.interest)
  if (req.body.action == "add") {
    let reponseArray = req.body.interest.map((val, key) => {
      let index = req.body.userData.interest.findIndex((e) => e == val.interestId)
      if (index > -1) {
        return false;
      } else {
        return true;
      }
    });
    console.log("reponseArray", reponseArray.findIndex((e) => e == true))
    if (reponseArray.findIndex((e) => e == true) > -1) {
      userController.addInterest(req, res)
    } else {
      return res.status(200).json({
        title: 'You have already selected the interest.',
        error: true,
        detail: [req.body.userData]
      });
    }
  } else {
    userController.addInterest(req, res)
  }
});

router.post('/fetchLeaderboardData/', [auth.authenticateUser, auth.isAuthenticated('fetchLeaderboardData')], (req, res, next) => {
  userController.fetchLeaderboardData(req, res)
});

router.get('/matchDetail/:playerId/:poolId', (req, res, next) => {
  userController.matchDetail(req, res)
});

router.post('/removeUser', [auth.authenticateUser, auth.isAuthenticated('removeUser')], (req, res, next) => {
  userController.removeUser(req, res)
});

router.post('/poolnameExist', (req, res, next) => {
  userController.poolnameExist(req, res)
});

router.post('/addMessage', (req, res, next) => {
  userController.addMessage(req, res);
})

router.post('/addGameDetail', (req, res, next) => {
  userController.addGameDetail(req, res);
})

router.post('/deleteGameDetail', (req, res, next) => {
  userController.deleteGameDetail(req, res);
})

router.post('/gameIdExist', (req, res, next) => {
  userController.gameIdExist(req, res);
});

router.post('/getPoolJoinValidity', (req, res) => {
  userController.getPoolJoinValidity(req, res);
})

router.post('/updateGames', (req, res) => {
  userController.updateGames(req, res);
})

router.get('/getMessages/:poolid', (req, res) => {
  console.log("req.///0", req.body, req.params);

  userController.getMessages(req, res);
})

router.post('/removePick', (req, res) => {
  userController.removePick(req, res);
})

router.post('/getMatchDetails', (req, res) => {
  userController.getMatchDetails(req, res);
})

module.exports = router;
