const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: "string",
    required: true
  },
  username: {
    type: "string",
    required: true
  },
  email: {
    type: "string",
    required: true,
    unique: true
  },
  mobile: {
    type: "string"
  },
  password: {
    type: "string",
    required: true
  },
  dob: {
    type: "string"
  },
  profileImage: {
    type: "string"
  },
  gender: {
    type: "string",
    enum: ['male', 'female', 'other']
  },
  address: {
    type: "string"
  },
  age: {
    type: "string"
  },
  role: {
    type: "string",
    default: "user"
  },
  isBlocked: {
    type: "number",
    default: 0
  },
  resetPasswordToken: {
    type: "string"
  },
  resetPasswordExpires: {
    type: "date"
  },
  interest: [{
    type: Schema.Types.ObjectId,
    ref: 'gameFormat'
  }],
  isActive: {
    type: Boolean,
    default: true
  }
},
  {
    timestamps: true
  });

module.exports = mongoose.model('user', schema);