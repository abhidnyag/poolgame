const mongoose = require('mongoose');
var findOrCreate = require('mongoose-findorcreate')
const Schema = mongoose.Schema;

const schema = new Schema({
  teamName: {
    type: "string"
  },
  logoUrl: {
    type: "string"
  },
  shortCode: {
    type: "string"
  },
  teamId: {
    type: "string"
  },
  city: {
    type: "string"
  },
  division: {
    type: "string"
  }
},
  {
    timestamps: true
  });

schema.plugin(findOrCreate);
module.exports = mongoose.model('team', schema);