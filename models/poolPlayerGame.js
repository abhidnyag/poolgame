const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  playerId: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  matchId: {
    type: Number
  },
  teamSelected: {
    type: Schema.Types.ObjectId,
    ref: 'team'
  },
  poolId: {
    type: Schema.Types.ObjectId,
    ref: 'pool'
  },
  multiPick: {
    type: Array
  },
  editableUpto: {
    type: "date"
  },
  isProvisional: {
    type: Boolean,
    deafult: false
  },
  matchDate: {
    type: String
  },
  score: {
    type: 'number',
    default: -1
  }
},
  {
    timestamps: true
  });

schema.index({ matchId: 1 });
module.exports = mongoose.model('poolPlayerGame', schema);