const mongoose = require('mongoose');
var findOrCreate = require('mongoose-findorcreate')
const Schema = mongoose.Schema;

const schema = new Schema({
  gameId: {
    type: Number
  },
  homeTeam: {
    type: Schema.Types.ObjectId,
    ref: 'team'
  },
  awayTeam: {
    type: Schema.Types.ObjectId,
    ref: 'team'
  },
  day: {
    type: "Date"
  },
  dateTime: {
    type: "Date"
  },
  winner: {
    type: Schema.Types.ObjectId,
    ref: 'team'
  },
  season: {
    type: Number
  },
  seasonType: {
    type: Number
  },
  status: {
    type: String
  },
  reScheduledGameId: {
    type: Number
  },
  awayScore: {
    type: String
  },
  homeScore: {
    type: String
  }
},
  {
    timestamps: true
  });

schema.plugin(findOrCreate);
schema.index({ gameId: 1 });
module.exports = mongoose.model('match', schema);