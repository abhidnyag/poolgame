const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  poolId: {
    type: Schema.Types.ObjectId,
    ref: 'pool'
  },
  multiPick: {
    type: Array
  },
  selected: [{
    type: Schema.Types.ObjectId,
    ref: 'team'
  }],
  selectionForMonth: {
    type: String
  }
},
  {
    timestamps: true
  });

module.exports = mongoose.model('availableTeams', schema);