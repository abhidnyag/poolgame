const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  title: {
    type: "string"
  },
  discription: {
    type: "string"
  },
  code: {
    type: "string"
  }
},
  {
    timestamps: true
  });

module.exports = mongoose.model('gameFormat', schema);