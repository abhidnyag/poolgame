const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//pool player mapping
const schema = new Schema({
  playerId: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  poolId: {
    type: Schema.Types.ObjectId,
    ref: 'pool'
  },
  status: {
    type: Boolean,
    default: true
  },
  wins: {
    type: Number,
    default: 0
  },
  loss: {
    type: Number,
    default: 0
  }
},
  {
    timestamps: true
  });

module.exports = mongoose.model('standing', schema);