const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    message: {
        type: "string"
    },
    name: {
        type: "string"
    },
    channelId: {
        type: Schema.Types.ObjectId,
        ref: 'pool'
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
},
    {
        timestamps: true
    });

module.exports = mongoose.model('chat', schema);