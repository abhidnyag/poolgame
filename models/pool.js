const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  poolName: {
    type: "string",
    required: true
  },
  month: {
    type: "date",
    // required: true
  },
  noPickDays: {
    type: Array
  },
  maxPlayers: {
    type: "string",
    // required: true
  },
  isActive: {
    type: "boolean",
    default: true
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  inviteCode: {
    type: "string",
    required: true
  },
  status: {
    type: String,
    enum: ['Open', 'Completed', 'Tied'],
    default: 'Open'
  },
  winner: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  multiPick: {
    type: Array
  },
  gameType: {
    type: Schema.Types.ObjectId,
    ref: 'gameFormat'
  },
  tiedPlayers: [{
    type: Schema.Types.ObjectId,
    ref: 'user'
  }]
},
  {
    timestamps: true
  });

module.exports = mongoose.model('pool', schema);